function Mirror_OnUpdate()
    if this.timer == nil then
        this:Hide()
        return
    end
    this:SetScale(ActionMirroringSettings.scale)
    Mirror_OnUpdateButton(arg1)
    
    if this:isCurrent() then
        getglobal(this:GetName().."HighlightTexture"):SetVertexColor(unpack(ActionMirroringSettings.activeColor))
    end
    
    if this.timer + arg1 > ActionMirroringSettings.timeout and not this:hold() then
        this:Hide()
    else
        this.timer = this.timer + arg1
    end
    
    Mirror_UpdateState()
end

function Mirror_OnLoad()
    
    this.flashing = 0
    this.flashtime = 0
    Mirror_Update()
    this:RegisterEvent("PLAYER_ENTERING_WORLD")
    this:RegisterEvent("UPDATE_BONUS_ACTIONBAR")
    this:RegisterEvent("ACTIONBAR_PAGE_CHANGED")
    this:RegisterEvent("ACTIONBAR_SLOT_CHANGED")
    this:RegisterEvent("UPDATE_BINDINGS")

    Mirror_UpdateHotkeys()
end

function Mirror_Update()
    -- Special case code for bonus bar buttons
    -- Prevents the button from updating if the bonusbar is still in an animation transition
    if ( this.isBonus and this.inTransition ) then
        Mirror_UpdateUsable()
        Mirror_UpdateCooldown()
        return
    end
    
    local icon = getglobal(this:GetName().."Icon")
    local buttonCooldown, texture
    if this.spell then
        buttonCooldown = GetSpellCooldown(this.spell, BOOKTYPE_SPELL)
        texture = GetSpellTexture(this.spell, BOOKTYPE_SPELL)
    elseif this.inventory then
        buttonCooldown = GetInventoryItemCooldown("player", this.inventory)
        texture = GetInventoryItemTexture("player", this.inventory)
    elseif this.container and GetContainerItem(this.container.bag,this.container.slot) == this.container.link then
        buttonCooldown = GetContainerItemCooldown(this.container.bag, this.container.slot)
        texture = GetContainerItemTexture(this.container.bag, this.container.slot)
    else
        buttonCooldown = getglobal(this:GetName().."Cooldown")
        local id = this:GetID()
        if id >= 0  then
            texture = GetActionTexture(id)
        else
            texture = GetShapeshiftFormInfo(-id)
        end
    end
    if ( texture ) then
        icon:SetTexture(texture)
        icon:Show()
        this.rangeTimer = -1
        getglobal(this:GetName().."NormalTexture"):SetTexture("Interface\\Addons\\ActionMirroringFrame\\ButtonBorder")
        -- Save texture if the button is a bonus button, will be needed later
        if ( this.isBonus ) then
            this.texture = texture
        end
    else
        icon:Hide()
        buttonCooldown:Hide()
        this.rangeTimer = nil
        this:Hide()
    end
    Mirror_UpdateCount()
    local id = this:GetID()
    if id < 0 or ( HasAction(id) ) then
        this:RegisterEvent("ACTIONBAR_UPDATE_STATE")
        this:RegisterEvent("ACTIONBAR_UPDATE_USABLE")
        this:RegisterEvent("ACTIONBAR_UPDATE_COOLDOWN")
        this:RegisterEvent("UPDATE_INVENTORY_ALERTS")
        this:RegisterEvent("PLAYER_AURAS_CHANGED")
        this:RegisterEvent("PLAYER_TARGET_CHANGED")
        this:RegisterEvent("UNIT_INVENTORY_CHANGED")
        this:RegisterEvent("CRAFT_SHOW")
        this:RegisterEvent("CRAFT_CLOSE")
        this:RegisterEvent("TRADE_SKILL_SHOW")
        this:RegisterEvent("TRADE_SKILL_CLOSE")
        this:RegisterEvent("PLAYER_ENTER_COMBAT")
        this:RegisterEvent("PLAYER_LEAVE_COMBAT")
        this:RegisterEvent("START_AUTOREPEAT_SPELL")
        this:RegisterEvent("STOP_AUTOREPEAT_SPELL")
        this:RegisterEvent("SPELL_UPDATE_USABLE")

        this:Show()
        Mirror_UpdateState()
        Mirror_UpdateUsable()
        Mirror_UpdateCooldown()
        Mirror_UpdateFlash()
    else
        this:UnregisterEvent("ACTIONBAR_UPDATE_STATE")
        this:UnregisterEvent("ACTIONBAR_UPDATE_USABLE")
        this:UnregisterEvent("ACTIONBAR_UPDATE_COOLDOWN")
        this:UnregisterEvent("UPDATE_INVENTORY_ALERTS")
        this:UnregisterEvent("PLAYER_AURAS_CHANGED")
        this:UnregisterEvent("PLAYER_TARGET_CHANGED")
        this:UnregisterEvent("UNIT_INVENTORY_CHANGED")
        this:UnregisterEvent("CRAFT_SHOW")
        this:UnregisterEvent("CRAFT_CLOSE")
        this:UnregisterEvent("TRADE_SKILL_SHOW")
        this:UnregisterEvent("TRADE_SKILL_CLOSE")
        this:UnregisterEvent("PLAYER_ENTER_COMBAT")
        this:UnregisterEvent("PLAYER_LEAVE_COMBAT")
        this:UnregisterEvent("START_AUTOREPEAT_SPELL")
        this:UnregisterEvent("STOP_AUTOREPEAT_SPELL")
        this:UnregisterEvent("SPELL_UPDATE_USABLE")

        this:Hide()
    end

    -- Add a green border if button is an equipped item
    local border = getglobal(this:GetName().."Border")
    if id > 0 and ( IsEquippedAction(id) ) then
        border:SetVertexColor(0, 1.0, 0, 0.35)
        border:Show()
    else
        border:Hide()
    end

    -- Update Macro Text
    local macroName = getglobal(this:GetName().."Name")
    if id > 0 and ActionMirroringSettings.macroText then
        macroName:SetText(GetActionText(id))
        macroName:Show()
    else
        macroName:Hide()
    end
end


function Mirror_OnEvent(event)
    if ( event == "ACTIONBAR_SLOT_CHANGED" ) then
        if ( arg1 == 0 or arg1 == this:GetID() ) then
            Mirror_Update()
        end
        return
    end
    if ( event == "PLAYER_ENTERING_WORLD" or event == "ACTIONBAR_PAGE_CHANGED" ) then
        Mirror_Update()
        return
    end
    if ( event == "UPDATE_BONUS_ACTIONBAR" ) then
        if ( this.isBonus ) then
            Mirror_Update()
        end
        return
    end
    if ( event == "UPDATE_BINDINGS" ) then
        Mirror_UpdateHotkeys()
        return
    end

    -- All event handlers below this line are only set when the button has an action

    if ( event == "PLAYER_TARGET_CHANGED" or event == "PLAYER_AURAS_CHANGED" ) then
        Mirror_UpdateUsable()
        Mirror_UpdateHotkeys()
    elseif ( event == "UNIT_INVENTORY_CHANGED" ) then
        if ( arg1 == "player" ) then
            Mirror_Update()
        end
    elseif ( event == "ACTIONBAR_UPDATE_STATE" ) then
        Mirror_UpdateState()
    elseif ( event == "SPELL_UPDATE_USABLE" or event == "ACTIONBAR_UPDATE_USABLE" or event == "UPDATE_INVENTORY_ALERTS" or event == "ACTIONBAR_UPDATE_COOLDOWN" ) then
        Mirror_UpdateUsable()
        Mirror_UpdateCooldown()
    elseif ( event == "CRAFT_SHOW" or event == "CRAFT_CLOSE" or event == "TRADE_SKILL_SHOW" or event == "TRADE_SKILL_CLOSE" ) then
        Mirror_UpdateState()
    elseif ( event == "PLAYER_ENTER_COMBAT" ) then
        if this:GetID() > 0 and IsAttackAction(this:GetID()) then
            Mirror_StartFlash()
        end
    elseif ( event == "PLAYER_LEAVE_COMBAT" ) then
        if ( this:GetID() > 0 and IsAttackAction(this:GetID()) ) then
            Mirror_StopFlash()
        end
    elseif ( event == "START_AUTOREPEAT_SPELL" ) then
        if ( this:GetID() > 0 and IsAutoRepeatAction(this:GetID()) ) then
            Mirror_StartFlash()
        end
    elseif ( event == "STOP_AUTOREPEAT_SPELL" ) then
        if ( this:GetID() > 0 and Mirror_IsFlashing() and not IsAttackAction(this:GetID()) ) then
            Mirror_StopFlash()
        end
    end
end

function Mirror_UpdateFlash()
    local id = this:GetID()
    if id < 0 and ( (IsAttackAction(id) and IsCurrentAction(id)) or IsAutoRepeatAction(id) or (this.spell and IsCurrentCast(this.spell, BOOKTYPE_SPELL)) ) then
        Mirror_StartFlash()
    else
        Mirror_StopFlash()
    end
end

function Mirror_UpdateHotkeys()
    local hotkey = getglobal(this:GetName().."HotKey")
    if ActionMirroringSettings.hotkeyText then
        hotkey:Show()
    else
        hotkey:Hide()
        return
    end
    local button = this:GetID()
    local bar = 6 - math.floor((button-1)/12)
    if bar == 4 then
        bar = 3
    elseif bar == 3 then
        bar = 4
    end
    local actionButtonType = (bar < 1 or button <= 12) and "ACTIONBUTTON" or ("MULTIACTIONBAR"..bar.."BUTTON")
    local action = actionButtonType..(mod(button -1,12)+1)
    local key = GetBindingText(GetBindingKey(action), "KEY_", 1)
    if ( key == "" ) then
        hotkey:SetText("")
        if ( ActionHasRange(button) ) then
            if ( IsActionInRange(button) ) then
                hotkey:SetText(RANGE_INDICATOR)
                hotkey:SetTextHeight(8)
                hotkey:SetPoint("TOPRIGHT", this:GetName(), "TOPRIGHT", -3, 5)         
            end
        end
    else
        hotkey:SetText(key)
    end
end


function Mirror_OnUpdateButton(elapsed)

    local hotkey = getglobal(this:GetName().."HotKey")
    local button = this:GetID()

    if ( hotkey:GetText() == RANGE_INDICATOR ) then
        if ( button > 0 and IsActionInRange(button) == 1 ) then
            hotkey:Hide()
        else
            hotkey:Show()
        end
    end

    if ( Mirror_IsFlashing() ) then
        this.flashtime = this.flashtime - elapsed
        if ( this.flashtime <= 0 ) then
            local overtime = -this.flashtime
            if ( overtime >= ATTACK_BUTTON_FLASH_TIME ) then
                overtime = 0
            end
            this.flashtime = ATTACK_BUTTON_FLASH_TIME - overtime

            local flashTexture = getglobal(this:GetName().."Flash")
            if ( flashTexture:IsVisible() ) then
                flashTexture:Hide()
            else
                flashTexture:Show()
            end
        end
    end
    
    -- Handle range indicator
    if ( this.rangeTimer ) then
        this.rangeTimer = this.rangeTimer - elapsed

        if ( this.rangeTimer <= 0 ) then
            local count = getglobal(this:GetName().."HotKey")
            if button > 0 and IsActionInRange( this:GetID()) == 0 then
                count:SetVertexColor(1.0, 0.1, 0.1)
            else
                count:SetVertexColor(0.6, 0.6, 0.6)
            end
            this.rangeTimer = TOOLTIP_UPDATE_TIME
        end
    end

    if ( not this.updateTooltip ) then
        return
    end

    this.updateTooltip = this.updateTooltip - elapsed
    if ( this.updateTooltip > 0 ) then
        return
    end
end


function Mirror_StartFlash()
    this.flashing = 1
    this.flashtime = 0
    Mirror_UpdateState()
end

function Mirror_StopFlash()
    this.flashing = 0
    getglobal(this:GetName().."Flash"):Hide()
    Mirror_UpdateState()
end

function Mirror_IsFlashing()
    if ( this.flashing == 1 ) then
        return 1
    else
        return nil
    end
end

function Mirror_UpdateState()
    this:SetChecked(this.timer <= ActionMirroringSettings.flashtime or this:isCurrent())
end

function Mirror_UpdateUsable()
    local icon = getglobal(this:GetName().."Icon")
    local normalTexture = getglobal(this:GetName().."NormalTexture")
    local id, isUsable, notEnoughMana = this:GetID()
    if id >= 0 then
        isUsable, notEnoughMana = IsUsableAction(id)
    else
        local _,_, active, castable = GetShapeshiftFormInfo(-id)
        isUsable = castable
        notEnoughMana = not castable and not active
    end
    if ( isUsable ) then
        icon:SetVertexColor(1.0, 1.0, 1.0)
        normalTexture:SetVertexColor(1.0, 1.0, 1.0)
    elseif ( notEnoughMana ) then
        icon:SetVertexColor(0.5, 0.5, 1.0)
        normalTexture:SetVertexColor(0.5, 0.5, 1.0)
        if ActionMirroringSettings.costTip then
            getglobal(this:GetName().."PowerTip"):Show()
        end
    else
        icon:SetVertexColor(0.4, 0.4, 0.4)
        normalTexture:SetVertexColor(1.0, 1.0, 1.0)
    end
end

function Mirror_UpdateCount()
    local text = getglobal(this:GetName().."Count")
    local id = this:GetID()
    if id > 0 and IsConsumableAction(id) then
        text:SetText(GetActionCount(this:GetID()))
    else
        text:SetText("")
    end
end

function Mirror_UpdateCooldown()
    local start, duration, enable
    if this.spell then
        start, duration, enable = GetSpellCooldown(this.spell,BOOKTYPE_SPELL)
    elseif this.inventory then
        start, duration, enable = GetInventoryItemCooldown("player", this.inventory)
    elseif this.container and GetContainerItem(this.container.bag,this.container.slot) == this.container.link then
        start, duration, enable = GetContainerItemCooldown(this.container.bag,this.container.slot)
    else
        local id = this:GetID()
        if id > 0 then
            start, duration, enable = GetActionCooldown(id)
        else
            start, duration, enable = GetShapeshiftFormCooldown(-id)
        end
    end
    local cooldown = getglobal(this:GetName().."Cooldown")
    CooldownFrame_SetTimer(cooldown, start, duration, enable)
    if ActionMirroringSettings.cooldownTip and GetTime() - start < duration and (duration > ActionMirroringSettings.cooldownTipThreshold) then
        getglobal(this:GetName().."CooldownTip"):Show()
    end
end

function Mirror_Refresh(self)
    local oldthis = this
    this = self
    Mirror_UpdateFlash()
    Mirror_Update()
    Mirror_UpdateState()
    Mirror_UpdateHotkeys(this.buttonType)
    getglobal(this:GetName().."HighlightTexture"):SetVertexColor(unpack(ActionMirroringSettings.clickColor))
    this = oldthis
end

function Mirror_UpdateCooldownTip()
    local cooldown = getglobal(this:GetParent():GetName().."Cooldown")
    if not cooldown:IsShown() or cooldown.stopping ~= 0 or cooldown.duration <= ActionMirroringSettings.cooldownTipThreshold then
        this:Hide()
        return
    end
    local left = cooldown.duration - GetTime() + cooldown.start
    if left <= 60 then
        getglobal(this:GetName().."Text"):SetText(string.format("%.1fs", left))
    else
        getglobal(this:GetName().."Text"):SetText(string.format("%d:%02d", left/60, mod(left,60)))
    end
end

function Mirror_UpdatePowerTip()
    local id = this:GetParent():GetID()
    local noMana
    if id >= 0 then
        _, noMana = IsUsableAction(id)
    else
        local _,_, active, usable = GetShapeshiftFormInfo(-id)
        noMana = not usable and not active
    end
    if noMana then
        this.update = this.update and this.update - arg1 or TOOLTIP_UPDATE_TIME
        if this.id == id and this.update > 0 then
            return
        end
        this.id = id
        this.update = TOOLTIP_UPDATE_TIME
        ActionMirroringFrameTooltipScanner:SetOwner(UIParent, "ANCHOR_NONE")
        local spell = this:GetParent().spell
        local inventory = this:GetParent().inventory
        local container = this:GetParent().container
        if spell then
            ActionMirroringFrameTooltipScanner:SetSpell(spell,BOOKTYPE_SPELL)
        elseif inventory then
            ActionMirroringFrameTooltipScanner:SetInventoryItem("player", inventory)
        elseif container then
            ActionMirroringFrameTooltipScanner:SetContainerItem(container.bag, container.slot)
        else
            if id >= 0 then
                ActionMirroringFrameTooltipScanner:SetAction(id)
            else
                ActionMirroringFrameTooltipScanner:SetShapeshift(-id)
            end
        end
        local powerType
        local text = getglobal(this:GetName().."Text")
        local background = getglobal(this:GetName().."Background")
        local tip = ActionMirroringFrameTooltipScannerTextLeft2:GetText()
        if tip == nil then
            text:Hide()
        end
        local _,_,req, t = strfind(tip or "","(%d+).-(%w+)")
        if t == MANA then
            powerType = 0
        elseif t == RAGE then
            powerType = 1
        elseif t == ENERGY then
            powerType = 3
        end
        if powerType and UnitPowerType("player") == powerType then
            text:SetText(tonumber(req) - UnitMana("player"))
            text:Show()
        else
            text:Hide()
        end
        if powerType == 0 then
            background:SetVertexColor(0,0,1)
            background:Show()
        elseif powerType == 1 then
            background:SetVertexColor(1,0,0)
            background:Show()
        elseif powerType == 3 then
            background:SetVertexColor(1,1,0)
            background:Show()
        else
            background:Hide()
        end
    else
        this:Hide()
    end
end

function MirrorTip_restorePoints(this)
    this:ClearAllPoints()
    for _,point in this:GetParent().revertedTips and this.revertedPoints or this.points do
        this:SetPoint(unpack(point))
    end
end

function MirrorTip_restoreAltPoints(this)
    this:ClearAllPoints()
    for _,point in this.altPoints do
        this:SetPoint(unpack(point))
    end
end

function Mirror_changeTipsPosition(tips, p)
    tips:ClearAllPoints()
    if mod(p,2) == 0 then
        tips:SetPoint("CENTER", tips:GetParent():GetName(), "CENTER")
    else
        tips:SetPoint("BOTTOM", tips:GetParent():GetName(), "BOTTOM")
    end
    if p == 0 then
        tips:SetPoint("BOTTOM", tips:GetParent():GetName(), "BOTTOM")
    elseif p == 1 then
        tips:SetPoint("LEFT", tips:GetParent():GetName(), "RIGHT", 3, 0)
    elseif p == 2 then
        tips:SetPoint("TOP", tips:GetParent():GetName(), "BOTTOM", 0, -4)
    elseif p == 3 then
        tips:SetPoint("RIGHT", tips:GetParent():GetName(), "LEFT", -3,0)
    elseif p == 4 then
        tips:SetPoint("BOTTOM", tips:GetParent():GetName(), "TOP", 0, 4)
    end
    tips:GetParent().revertedTips = p == 2
    local chained = tips.next
    local shown = tips:IsShown()
    while chained do
        if shown then
            MirrorTip_restorePoints(chained)
        else
            MirrorTip_restoreAltPoints(chained)
        end
        shown = chained:IsShown()
        chained = chained.next
    end
end
